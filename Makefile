all:
	g++ -pthread -std=c++11 -o server.o server/main.cpp server/Server.cpp shared/SimpleChat.h shared/StringUtils.hpp
	g++ -pthread -std=c++11 -o client.o client/main.cpp shared/SimpleChat.h shared/StringUtils.hpp
clean:
	rm *.o

