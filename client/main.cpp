#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <thread>

#include "../shared/SimpleChat.h"

void receiveLogic(int sock) {
    char buffer[SimpleChat::BUFFER_SIZE];
    for (;;) {
        bzero(buffer, sizeof(buffer));
        ssize_t n = read(sock, buffer, sizeof(buffer) - 1);
        if (n <= 0) {
            std::cout << "Socket closed" << std::endl;
            break;
        }
        std::cout << buffer << std::endl;
    }
    close(sock);
    exit(0);
}

bool sendServer(int sock, const std::string &message) {
    ssize_t n = write(sock, message.data(), message.size());
    if (n < 0) {
        perror("ERROR writing to socket");
        return false;
    }
    return true;
}

int main(int argc, char **argv) {
    if (argc < 2) {
        std::cout << "Usage:" << std::endl
                  << "    SimpleChatClient [options]" << std::endl
                  << "Options:" << std::endl
                  << "    -p [ports]    ports to connect to, w. port knocking order" << std::endl
                  << "    -h [host]     host to connect to" << std::endl;
        return 0;
    }

    // Parse arguments
    SimpleChat::ServerOptions serverOptions = SimpleChat::parseServerOptions(argc, argv);

    std::string username;
    std::cout << "Enter username to use (can be changed with CONNECT" << SimpleChat::COMMAND_DELIM << "<username>): ";
    std::cin >> username;

    int main_socket;
    int first_port = atoi(serverOptions.ports[0].data()),
            mid_port = atoi(serverOptions.ports[1].data()),
            main_port = atoi(serverOptions.ports[2].data());
    sockaddr_in serv_addr{};
    hostent *server = gethostbyname(serverOptions.address.data());

    main_socket = socket(AF_INET, SOCK_STREAM, 0);

    if (server == nullptr) {
        perror("Error no such host");
        exit(1);
    }

    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;

    // Host address is stored in network byte order
    bcopy((char *) server->h_addr,
          (char *) &serv_addr.sin_addr.s_addr,
          static_cast<size_t>(server->h_length));

    serv_addr.sin_port = htons(first_port);
    if (connect(main_socket, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
        perror("ERROR connecting");
    close(main_socket);
    main_socket = socket(AF_INET, SOCK_STREAM, 0);
    serv_addr.sin_port = htons(mid_port);
    if (connect(main_socket, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
        perror("ERROR connecting");
    close(main_socket);
    main_socket = socket(AF_INET, SOCK_STREAM, 0);
    serv_addr.sin_port = htons(main_port);
    if (connect(main_socket, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
        perror("ERROR connecting");

    std::stringstream to_send;
    to_send << "CONNECT" << SimpleChat::COMMAND_DELIM << username;
    if (!sendServer(main_socket, to_send.str())) {
        perror("Unable to set username");
        exit(1);
    }

    char buffer[SimpleChat::BUFFER_SIZE];
    std::thread rec_thread(receiveLogic, main_socket);
    rec_thread.detach();
    for (;;) {
        bzero(buffer, sizeof(buffer));
        std::cin >> buffer;
        bool worked = sendServer(main_socket, buffer);
        if (!worked) {
            break;
        }
    }

    close(main_socket);
    return 0;
}