#ifndef __RAND_UTILS_H_
#define __RAND_UTILS_H_

#include <random>

static thread_local std::mt19937 rng;

class RandUtils {
public:
    /**
     * Random number generator for each thread
     */
    static std::mt19937 getRng() { return rng; }

    /**
     * Seeds the generator using the random_device
     */
    static void seedGenerator() { rng.seed(std::random_device()()); }

    /**
     * Generates a random number between min and max, inclusive.
     */
    static int randInt(const int &min, const int &max) {
        std::uniform_int_distribution<int> distribution(min, max);
        return distribution(rng);
    }
};

#endif // __RAND_UTILS_H_
