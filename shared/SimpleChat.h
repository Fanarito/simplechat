#ifndef __SIMPLE_CHAT_H_
#define __SIMPLE_CHAT_H_

#include <string>
#include <vector>
#include <cstring>
#include <iostream>

#include "StringUtils.hpp"

class SimpleChat {
public:
    struct ServerOptions {
        std::string address;
        std::vector<std::string> ports;
    };

    static const char COMMAND_DELIM = '|';
    static const int BUFFER_SIZE = 1024;

    static ServerOptions parseServerOptions(int argc, char **argv) {
        SimpleChat::ServerOptions serverOptions = SimpleChat::ServerOptions();
        for (int i = 1; i < argc; i++) {
            if (std::strncmp(argv[i], "-p", 2) == 0) {
                i++;
                serverOptions.ports = StringUtils::split(argv[i], ',');
            } else if (std::strncmp(argv[i], "-h", 2) == 0) {
                i++;
                serverOptions.address = argv[i];
            }
        }

        if (serverOptions.ports.size() != 3) {
            std::cout << "3 ports required" << std::endl;
            exit(1);
        }
        return serverOptions;
    }

}; // namespace SimpleChat

#endif // __SIMPLE_CHAT_H_