# SimpleChat

## Build Instructions
```sh
mkdir build && cd build
cmake ..
make
```

## Running Server
```
Usage:
    SimpleChatServer [options]
Options:
    -p [ports]    ports to listen on, w. port knocking order
```

## Running Client
```
Usage:
    SimpleChatClient [options]
Options:
    -p [ports]    ports to connect to, w. port knocking order
    -h [host]     host to connect to
```

# Commands

Sending an invalid command will result in a closed connection.
This is by design.

- `ID` sends the servers ID to the client
- `CONNECT|<username>` sets the username of the client
- `LEAVE` closes the connection on the server
- `WHO` lists users on the server
- `MSG|<username>|<msg>` sends a private message to user
- `MSG|ALL|<msg>` sends a message to all users
- `CHANGE|ID` regenerates the id of the server

# Help material used

- Beej's guide to networking

Successfully compiles on OpenSuse Tumbleweed 20180926 snapshot
