#include "Server.h"

#include <utility>
#include <strings.h>
#include <future>
#include <netdb.h>
#include <cstring>

Server::Server(const SimpleChat::ServerOptions &serverOptions) : serverOptions(serverOptions) {
    reloadId();
}

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa) {
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in *) sa)->sin_addr);
    }

    return &(((struct sockaddr_in6 *) sa)->sin6_addr);
}

void Server::start() {
    int newfd;
    sockaddr_storage remoteaddr{};
    socklen_t addrlen;

    char buf[SimpleChat::BUFFER_SIZE];

    char remoteIP[INET6_ADDRSTRLEN];

    int rv;

    FD_ZERO(&master);
    FD_ZERO(&read_fds);

    bindSocket(firstKnock, serverOptions.ports.at(0));
    bindSocket(midKnock, serverOptions.ports.at(1));
    bindSocket(listener, serverOptions.ports.at(2));

    // Add listener to master set
    FD_SET(firstKnock, &master);
    FD_SET(midKnock, &master);
    FD_SET(listener, &master);

    // Keep track of the biggest file descriptor
    fdmax = listener;

    // Receive loop
    for (;;) {
        bzero(buf, sizeof(buf));
        read_fds = master;
        // Wait for activity in set
        if (select(fdmax + 1, &read_fds, nullptr, nullptr, nullptr) == -1) {
            perror("select");
            exit(4);
        }

        // Run through all connections and check for activity and
        // handle it if there is any.
        for (int i = 0; i <= fdmax; i++) {
            if (FD_ISSET(i, &read_fds)) {
                if (i == listener) {
                    portKnocks[2] = time(nullptr);

                    // If there is activity on the listener socket there is a new connection
                    addrlen = sizeof remoteaddr;
                    newfd = accept(listener, (struct sockaddr *) &remoteaddr, &addrlen);
                    if (newfd == -1) {
                        perror("accept");
                    } else {
                        // No port knock detected close socket
                        if (!hasPortKnocked()) {
                            close(newfd);
                            continue;
                        }

                        // Connection created, setup all needed data.
                        FD_SET(newfd, &master);
                        // Keep track of the max
                        if (newfd > fdmax) {
                            fdmax = newfd;
                        }
                        printf("selectserver: new connection from %s on socket %d\n",
                               inet_ntop(remoteaddr.ss_family,
                                         get_in_addr((struct sockaddr *) &remoteaddr),
                                         remoteIP, INET6_ADDRSTRLEN),
                               newfd);
                        Client client;
                        client.sock = newfd;
                        client.username = "default" + std::to_string(newfd);
                        clients.insert(std::pair<int, Client>(client.sock, client));
                    }
                } else if (i == firstKnock) {
                    portKnocks[0] = time(nullptr);
                } else if (i == midKnock) {
                    portKnocks[1] = time(nullptr);
                } else {
                    // In this case there is a message from a client, either
                    // a new message or a closed connection
                    int nbytes;
                    if ((nbytes = recv(i, buf, sizeof buf, 0)) <= 0) {
                        // Handle closed connection
                        if (nbytes == 0) {
                            printf("selectserver: socket %d hung up\n", i);
                        } else {
                            perror("recv");
                        }
                        endConnection(clients.find(i)->second);
                    } else {
                        // Handle received message
                        Client *sender = getClient(i);
                        processMessage(sender, buf);
                    }
                }
            }
        }
    }
}

void Server::broadcast(std::string message) {
    for (auto &client : clients) {
        int fd = client.first;
        if (FD_ISSET(fd, &master)) {
            if (send(fd, message.data(), message.size(), 0) == -1) {
                perror("send");
            }
        }
    }
}

Client *Server::getClient(int sock) {
    return &clients.find(sock)->second;
}

Client *Server::getClient(std::string username) {
    for (auto &client : clients) {
        if (client.second.username == username) {
            return &client.second;
        }
    }
    return nullptr;
}

void Server::sendAll(Client sender, std::string message) {
    broadcast(std::move(message));
}

void Server::sendUser(Client receiver, std::string message) {
    if (FD_ISSET(receiver.sock, &master)) {
        if (send(receiver.sock, message.data(), message.size(), 0) == -1) {
            perror("send");
        }
    }
}

void Server::processMessage(Client *sender, std::string message) {
    StringUtils::trim(message);
    std::vector<std::string> tokens = StringUtils::split(message, SimpleChat::COMMAND_DELIM);

    if (tokens.empty()) {
        return;
    }
    std::string command = tokens.at(0);

    if (command == "MSG") {
        std::string receiver = tokens.at(1);
        if (receiver == "ALL") {
            std::string to_send = sender->username + ":\n" + tokens.at(2) + "\n";
            sendAll(*sender, to_send);
        } else {
            std::string to_send = sender->username + " (private message):\n" + tokens.at(2) + "\n";
            Client *recipient = getClient(receiver);
            sendUser(*recipient, to_send);
        }
    } else if (command == "WHO") {
        std::string to_send;
        for (auto &user : clients) {
            to_send.append(user.second.username);
            to_send.append("\n");
        }
        sendUser(*sender, to_send);
    } else if (command == "LEAVE") {
        std::string to_send = "Goodbye\n";
        sendUser(*sender, to_send);
        endConnection(*sender);
    } else if (command == "CONNECT") {
        std::string username = tokens.at(1);
        std::string to_send = "Username: " + username + "\n";
        sender->username = username;
        sendUser(*sender, to_send);
    } else if (command == "ID") {
        std::string to_send = id;
        sendUser(*sender, to_send);
    } else if (command == "CHANGE") {
        if (tokens.at(1) == "ID") {
            reloadId();
        }
    } else {
        endConnection(*sender, true);
    }
}

void Server::endConnection(Client client, bool invalidCommand) {
    std::cout << "(" << client.sock << "," << client.username << "): disconnected "
              << invalidCommand << std::endl;
    close(client.sock);
    FD_CLR(client.sock, &master); // remove from master set
    clients.erase(client.sock);
}

bool Server::hasPortKnocked() {
    if (portKnocks[0] == 0 || portKnocks[1] == 0 || portKnocks[2] == 0) {
        return false;
    }

    long now = time(nullptr);
    long diff = 0;
    diff += now - portKnocks[2];
    diff += now - portKnocks[1];
    diff += now - portKnocks[0];
    return diff < 20 && portKnocks[2] >= portKnocks[1] && portKnocks[1] >= portKnocks[0];
}

void Server::bindSocket(int &s, std::string port) {
    int rv, yes = 1;
    addrinfo hints{}, *ai, *p;

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    if ((rv = getaddrinfo(nullptr, port.data(), &hints, &ai)) != 0) {
        fprintf(stderr, "selectserver: %s\n", gai_strerror(rv));
        exit(1);
    }

    for (p = ai; p != nullptr; p = p->ai_next) {
        s = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if (s < 0) {
            continue;
        }

        // lose the pesky "address already in use" error message
        setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));

        if (bind(s, p->ai_addr, p->ai_addrlen) < 0) {
            close(s);
            continue;
        }

        break;
    }

    // If p is a nullptr at this point bind has failed
    if (p == nullptr) {
        fprintf(stderr, "selectserver: failed to bind\n");
        exit(2);
    }

    freeaddrinfo(ai);


    if (listen(s, 10) == -1) {
        perror("listen");
        exit(3);
    }
}

void Server::reloadId() {
    std::array<char, 160> buffer{};
    std::shared_ptr<FILE> pipe(popen("fortune -s", "r"), pclose);
    std::string result;
    if (!pipe) throw std::runtime_error("popen() failed!");
    while (!feof(pipe.get())) {
        if (fgets(buffer.data(), 160, pipe.get()) != nullptr)
            result += buffer.data();
    }
    id = result + "VS " + std::to_string(time(nullptr));
}
