#include <iostream>
#include <cstring>
#include <string>
#include <vector>

#include "../shared/RandUtils.hpp"
#include "../shared/StringUtils.hpp"
#include "../shared/SimpleChat.h"
#include "Server.h"

int main(int argc, char **argv) {
    if (argc < 2) {
        std::cout << "Usage:" << std::endl
                  << "    SimpleChatServer [options]" << std::endl
                  << "Options:" << std::endl
                  << "    -p [ports]    ports to listen on, w. port knocking order" << std::endl;
        return 0;
    }

    // Parse arguments
    SimpleChat::ServerOptions serverOptions = SimpleChat::parseServerOptions(argc, argv);
    Server server(serverOptions);
    server.start();

    return 0;
}