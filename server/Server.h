//
// Created by fanarito on 29.9.2018.
//

#ifndef SIMPLECHAT_SERVER_H
#define SIMPLECHAT_SERVER_H

#include <iostream>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <map>

#include "../shared/SimpleChat.h"
#include "../shared/StringUtils.hpp"

struct Client {
    int sock;
    std::string username;
};

class Server {
private:
    SimpleChat::ServerOptions serverOptions;
    int fdmax, listener, firstKnock, midKnock;
    fd_set master;
    fd_set read_fds;
    std::map<int, Client> clients;
    long portKnocks[3]{0, 0, 0};
    std::string id;

    /**
     * Broadcasts message to all clients
     * @param message
     */
    void broadcast(std::string message);

    Client *getClient(int sock);

    Client *getClient(std::string username);

    /**
     * Sends all users on server
     * @param sender
     * @param message
     */
    void sendAll(Client sender, std::string message);

    /**
     * Sends a single user a message
     * @param receiver
     * @param message
     */
    void sendUser(Client receiver, std::string message);

    /**
     * Ends connection with client
     * @param client
     * @param invalidCommand
     */
    void endConnection(Client client, bool invalidCommand = false);

    /**
     * Processes message and does all the work
     * @param sender
     * @param message
     */
    void processMessage(Client *sender, std::string message);

    /**
     * Checks if there has been a port knock recently
     * @return
     */
    bool hasPortKnocked();

    /**
     * Binds socket to poert
     * @param s
     * @param port
     */
    void bindSocket(int &s, std::string port);

    /**
     * Sets the id
     */
    void reloadId();

public:
    explicit Server(const SimpleChat::ServerOptions &serverOptions);

    /**
     * Runs the server
     */
    void start();
};


#endif //SIMPLECHAT_SERVER_H
